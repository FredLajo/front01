import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores } from '/src/estilos-fred.js'

class TransactionPanel extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
  static get properties() {
    return {
      accountID: {type: String},
      accountNumber:{type:String},
      cci:{type:String},
      amountC:{type:String},
      amountD:{type:String},
      description:{type:String},
      data: {type: Object}
    }
  }
  constructor(){
    super();
    this.data = [];
    this.accountID=sessionStorage.getItem("accountID")==null?0:sessionStorage.getItem("accountID");
    this.accountNumber=sessionStorage.getItem("accountNumber");
    this.cci=sessionStorage.getItem("cci");
    this.description=sessionStorage.getItem("description");
    this.amountC=sessionStorage.getItem("amountC");
    this.amountD=sessionStorage.getItem("amountD");
    //sessionStorage.clear();
  }
  render() {
    return html`
    <style>
    #encabezadotabla{
      border:1px solid var(--lumo-contrast-20pct);
      padding:15px;
      border-radius:5px 5px 0px 0px;
      border-bottom: 0px;
      background-color: #18749F;
      position:sticky;
      top: 0;
    }
    .caja{
      background-color: #18749F;
      padding-top: 10px;
      padding-bottom:  10px;
      border:1px solid var(--lumo-contrast-20pct);
      border-top: 0px;
    }
    .descripcion{
      padding-left: 20px;
      overflow:hidden;
    }
    .monto{
      padding-right: 30px;
    }

    </style>
    <div id="encabezadotabla" class="white s16 ta_c ">
      <b>${this.description}</b>
    </div>
    <div class="caja white s14 contH w_100">
      <div id="descripcion" class="contV h_100 jc_c descripcion w_50 ai_s ">
        NÚMERO DE CUENTA
      </div>
      <div  class=" monto white contV h_100 jc_c w_50 ai_e">
        ${this.accountNumber}
      </div>
      <div  class=" descripcion contV h_100 jc_c w_50 ai_s">
        CCI
      </div>
      <div class=" monto white contV h_100 jc_c w_50 ai_e">
        ${this.cci}
      </div>
    </div>
    <div id="loader" class="contH w_100 hidden jc_c ai_c">
      <ripple-loader></ripple-loader>
    </div>
    <div id="error" class="contH w_100 hidden jc_c ai_c">
      no se pudo recuperar la data
    </div>
    <div id="transactionpanel" class="hidden" >

      ${this.data.map(datos => html`
          <transaction-data
            transacID="${datos.transactionID}"
            description="${datos.description}"
            amount="${datos.monto}"
            date="${datos.fecha}"
            tipo="${datos.tipo}"
            divisa="${datos.divisa}"
          > </transaction-data>
      `)}
    </div>
    <iron-ajax id="ajax" @response="${this.bringdata}" @request="${this.cargando}"
      @error="${this.error}"
        auto
       url = "http://localhost:3000/api-peru/v1/accounts/${this.accountID}/transactions"

       handle-as = "json"

       >
    </iron-ajax>
    `
  }
  error(){
    this.shadowRoot.getElementById("loader").classList.toggle('hidden');
    this.shadowRoot.getElementById("error").classList.toggle('hidden');
  }
  bringdata(data){
    //console.log("recibido1");
    this.shadowRoot.getElementById("loader").classList.toggle('hidden');
    //console.log("recibido2");
    if(data.detail.status==200){
      this.data=data.detail.response;
      //console.log(this.data);
      this.shadowRoot.getElementById("transactionpanel").classList.remove('hidden');
    }

  }
  cargando(){
    this.data=[];
    this.shadowRoot.getElementById("loader").classList.toggle('hidden');
  }
}

customElements.define('transaction-panel', TransactionPanel)



class TransactionData extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
  static get properties() {
    return {
      transacID: {type: String},
      description:{type:String},
      amount:{type:Number},
      date:{type:String},
      divisa:{type:String},
      tipo:{type:String}
    }
  }
  constructor(){
    super();
  }

  render() {
    return html`
    <style>
    .caja{
      height: 50px;
      border:1px solid var(--lumo-contrast-20pct);
      border-top: 0px;
    }
    #descripcion{
      padding-left: 20px;
      overflow:hidden;
    }
    #monto{
      padding-right: 30px;
    }

    </style>
      <div class="caja s14 contH w_100">
        <div id="descripcion" class="contV h_100 jc_c ai_s w_70">
          <div class="blue" style="text-overflow: ellipsis;">${this.description}</div>
          <div class="s12">${this.date}</div>
        </div>
        <div id="monto" class="contV h_100 jc_c ai_e s17 w_30">
          <div class="${this.tipo=='Cargo'?'red':'blue'}">${this.divisa=='PEN'?'S/.':'$/.'} ${this.amount}</div>
        </div>
      </div>
    `
  }
}

customElements.define('transaction-data', TransactionData)
