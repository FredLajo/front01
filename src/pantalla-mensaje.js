import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores} from '/src/estilos-fred.js'

class PantallaMensaje extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
    static get properties() {
      return {
        errorID:{type:String},
        mensajes:{type:Object},
        error:{type:String}
      }
    }
  constructor(){
    super();
    this.error="0";
    this.mensajes=["Saldo disponible insuficiente",
                    "Cuenta de cargo invalida",
                    "Cuenta de abono invalida",
                    "Monto no permitido",
                    "Cuenta de cargo vacia",
                    "Error inesperado",
                    "No se pudo crear cuenta",
                    "Creación exitosa",
                    "Nombre de cuenta inválido",
                    "Cargando...",
                    "Error al recuperar data del beneficiario",
                    "Error al realizar la transferencia",
                    "Transferencia exitosa",
                    "Divisa del beneficiario diferente a la divisa de la operacion"];

  }
  render() {
    return html`
    <style>
    .caja{
        z-index: 100;
        content: "";
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        pointer-events: auto;
        display: flex;

      }
      #mensaje{

      }
      .texto{
        width: 200px;
      }
      .fondo_e{
        background-color: #F83939;
      }
      .fondo_r{
        background-color: #EEEEEE;
      }

    </style>
      <div class="jc_c ai_c caja">
        <div id="mensaje" class="contV ai_c ">
        ${this.error=="0"?
            html`<img  src="https://www.freepngimg.com/thumb/artwork/84883-homer-emoticon-bart-area-marge-simpson.png"
                alt = "FRED PRODUCTION" width="120px" height="120px"/>
                <div class=" texto ta_c fondo_e br_5 p_10 white"><b>${this.mensajes[this.errorID]}</b></div>
            `:
            html`<img  src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/aaa79357-9476-4c1d-b9a8-6e881f2449d3/ddgdrd9-cb8622a9-d3a4-45a4-8b28-6968bbc71866.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvYWFhNzkzNTctOTQ3Ni00YzFkLWI5YTgtNmU4ODFmMjQ0OWQzXC9kZGdkcmQ5LWNiODYyMmE5LWQzYTQtNDVhNC04YjI4LTY5NjhiYmM3MTg2Ni5wbmcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.Sr5q0uu4n9ZSDVgTrk0Gz9wRfi98UmvlRA7abl0V82I"
                alt = "FRED PRODUCTION" width="120px" height="120px"/>
                <div class=" texto ta_c fondo_r br_estilo br_5 p_10 grey"><b>${this.mensajes[this.errorID]}</b></div>
            `}



        </div>
      </div>
    `
  }
}

customElements.define('pantalla-mensaje', PantallaMensaje)
