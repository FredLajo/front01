import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class ConfirmacionPage extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
  static get properties() {
    return {
      userID:{type:String},
      nameCargo: { type: String },
      nameAbono: { type: String },
      description: { type: String },
      divisa: { type: String },
      divisaCargo: { type: String },
      cuentaCargo:{type:String},
      cuentaAbono:{type:String},
      montoCargo:{type:Number},
      montoAbono:{type:Number},
      idError: { type: String },
      error: { type: String },
      tipo:{type:Number}
    }
  }
  constructor(){
    super();
    this.userID="";
    this.nameCargo="";
    this.nameAbono="";
    this.divisa="";
    this.divisaCargo="";
    this.cuentaCargo="";
    this.cuentaAbono="";
    this.montoCargo=0;
    this.montoAbono=0;
    this.description="";
    this.idError=-1;
    this.error="0";
    this.tipo=1;
  }
  render() {
    return html`
      <pantalla-mensaje id="alerta" errorID=${this.idError} error=${this.error} class="hidden"></pantalla-mensaje>
      <div class="contV w_100">
        <div class=" contH w_100 jc_c s16 white fondo_azul p_10 br_s_5">
          <b>CONFIRMAR OPERACION</b>
        </div>
        <mensaje-data titulo="Operacion" mensaje="${this.description}"></mensaje-data>
        <mensaje-data titulo="Cuenta de Cargo" mensaje="${this.cuentaCargo}"></mensaje-data>
        <mensaje-data titulo="Titular cuenta de Cargo" mensaje="${this.nameCargo}"></mensaje-data>
        <mensaje-data titulo="Cuenta de Abono" mensaje="${this.cuentaAbono}"></mensaje-data>
        <mensaje-data titulo="Beneficiario" mensaje="${this.nameAbono}"></mensaje-data>
        <mensaje-data titulo="Importe transferido" mensaje="${this.divisa=='PEN'?'S/.':'$/.'} ${this.montoAbono}"></mensaje-data>
        <mensaje-data titulo="Total cargado" mensaje="${this.divisaCargo=='PEN'?'S/.':'$/.'} ${this.montoCargo}"></mensaje-data>
        <vaadin-button @click="${this.confirmar}" class="w_100 b_confirmar m_t_10">Confirmar</vaadin-button>
      </div>
      <iron-ajax id="ajax" @response="${this.bringdata1}" @request="${this.cargando}"
        @error="${this.error}"
         url = "http://localhost:3000/api-peru/v1/users/${this.userID}/transactions"
         method = "put"
         handle-as = "json"
         content-type="application/json"
         >
      </iron-ajax>
    `
  }
  confirmar(){
    let ajax=this.shadowRoot.getElementById("ajax");
    ajax.body={description : this.description,cta_ord : this.cuentaCargo,cta_ben : this.cuentaAbono,divisa : this.divisa,monto : this.montoAbono,tipo: this.tipo};
    //console.log(ajax.body);
    ajax.generateRequest();
  }
  error(){
    this.idError=11;
    this.error="0";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
    setTimeout(()=>{
      this.shadowRoot.getElementById("alerta").classList.add('hidden');
    },2000);
  }
  cargando(){
    this.idError=9;
    this.error="1";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
  }
  bringdata1(){
    this.idError=12;
    this.error="1";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
    setTimeout(()=>{
      this.shadowRoot.getElementById("alerta").classList.add('hidden');
      location.href = "#!/";
    },2000);
  }

}

customElements.define('confirmacion-page', ConfirmacionPage)



class MensajeData extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
  static get properties() {
    return {
      titulo:{type:String},
      mensaje: { type: String }
    }
  }
  constructor(){
    super();
    this.titulo="";
    this.mensaje="";
  }
  render() {
    return html`
      <div class="contV w_100 p_l_20 p_t_10">
        <div class="blue">${this.titulo}</div>
        <div class="grey">${this.mensaje}</div>
      </div>
    `
  }

}

customElements.define('mensaje-data', MensajeData)
