import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class UserData extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
  static get properties() {
    return {
      userID:{type:String},
      name: { type: String },
      apepat: { type: String },
      apemat: { type: String },
      datas: {type: Object},
      fecha:{type:String}
    }
  }
  constructor(){
    super();
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var f=new Date();
    this.fecha=diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
    this.datas = [];
  }
  render() {

    return html`
    <style>

    .data1{
      margin-top: 10px;
      margin-bottom: 10px;
    }
    .encabezadotabla{
      height:50px;
      border:1px solid var(--lumo-contrast-20pct);
      padding-left: 15px;
      padding-right: 15px;
      border-bottom: 0px;
    }


    </style>
    <custom-style>
      <style include="lumo-typography lumo-color">
    </style>
    </custom-style>
          <div style="text-align: right">${this.fecha}</div>
          <div style="margin-bottom:20px">
            <span class="blue"><b>BIENVENIDO</b></span> ${this.name} ${this.apepat} ${this.apemat}
          </div>

          <div class="data1" >
            <div class=" encabezadotabla s16 white fondo_azul br_s_5 contH ai_c jc_sb ">
              <b>CUENTAS</b>
              <a href="#!/new" class="a_blank">
                <iron-icon style="cursor:pointer; color:white"icon="vaadin:plus-circle"></iron-icon>
              </a>

            </div>
            <div id="loader" class="contH w_100 hidden jc_c ai_c">
              <ripple-loader></ripple-loader>
            </div>
            <div id="error" class="contH w_100 hidden jc_c ai_c">
              no se pudo recuperar la data
            </div>
            ${this.datas.map(datos => html`
                <cuenta-data
                  accountID="${datos.accountID}"
                  description="${datos.description}"
                  accountNumber="${datos.cuenta}"
                  cci="${datos.cci}"
                  amountC="${datos.balance}"
                  amountD="${datos.balance}"
                  divisa="${datos.divisa}"
                > </cuenta-data>
            `)}
          </div>
          <iron-ajax @response="${this.bringdata}" @request="${this.cargando}"
            @error="${this.error}"
             auto url = "http://localhost:3000/api-peru/v1/users/${this.userID}/accounts"
             handle-as = "json"

             >
          </iron-ajax>


    `

  }
  mostrarCuentaPanel(){
    this.shadowRoot.getElementById("crearCuentaPanel").classList.toggle('hidden');
  }
  cancelar(){
    this.shadowRoot.getElementById("crearCuentaPanel").classList.add('hidden');
  }
  error(){
    this.shadowRoot.getElementById("loader").classList.add('hidden');
    this.shadowRoot.getElementById("error").classList.remove('hidden');
  }
  bringdata(data){
    //console.log("recibido");
    this.shadowRoot.getElementById("loader").classList.toggle('hidden');
    if(data.detail.status==200){
      this.shadowRoot.getElementById("error").classList.add('hidden');
      this.datas=data.detail.response;
    }

  }
  cargando(){
    this.datas=[];
    this.shadowRoot.getElementById("loader").classList.toggle('hidden');
    //console.log("enviado");
  }

}

customElements.define('user-data', UserData)
