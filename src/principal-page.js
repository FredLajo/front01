import { LitElement, html } from 'lit-element';
import Navigo from 'https://unpkg.com/navigo@7.1.2/lib/navigo.es.js';



class PrincipalPage extends LitElement {
  static get properties() {
    return {
      route: { type: Object },
      userID:{type:String},
      name: { type: String },
      apepat: { type: String },
      apemat: { type: String },
      email: { type: String},
      password: { type: String},
      url: { type: String},
      logged: { type: Boolean },
      mostrarCuenta: { type: Boolean },
      drawer: { type:Boolean},
      description:{type:String}
    }
  }
  constructor() {
    super()
    this.userID=4;
    this.name=""
    this.apepat=""
    this.apemat=""
    this.email=""
    this.password=""
    this.url = ""
    this.logged=false;
    this.drawer=true;
    this.mostrarCuenta=true;
    this.description="Proyecto TechU nivel Practicioner"
    //this.setrouter();
  }
  render() {
    return html`
          <style>
          iron-icon {
            padding: 0.25em;
            width: var(--lumo-icon-size-s);
            height: var(--lumo-icon-size-s);
          }
          #container{
            content: "";
            position: absolute;
            top: 44px;
            left: 0;
            bottom: 0;
            right: 0;
            pointer-events: auto;
            display: flex;
            justify-content:center;
            padding-top: 10px;
          }

            @media only screen and (min-width: 600px) and (orientation: landscape){
              #caja{
                flex-direction: row;
                align-items: stretch;
                max-width: calc(var(--lumo-size-m) * 14);
                width: 100%;
              }
            }
            @media only screen and (max-width: 600px){
              #caja{
                width: 100%;
                margin: 10px;
              }
            }
            .ocultar{
              display:none;
            }
            html{
              --lumo-base-color: #557CFF;
              --lumo-primary-color: #57BBAD;
              --lumo-error-color-10pct: hsla(3, 100%, 60%, 0.2);
            }
        </style>

        <vaadin-login-overlay id="loginform" @login="${this.login}" opened title="TechU2020" description="${this.description}"></vaadin-login-overlay>
        <vaadin-app-layout primary-section="drawer" id="drawer">
          <vaadin-drawer-toggle slot="navbar" touch-optimized></vaadin-drawer-toggle>
          <h2 slot="navbar"></h2>
          <vaadin-tabs orientation="vertical"  slot="drawer" theme="minimal" style="margin: 0 auto; flex: 1;">
            <vaadin-tab>
              <a href="#!/" @click="${this.cerrardrawer}">
                <iron-icon icon="vaadin:home"></iron-icon>
                CUENTAS
              </a>
            </vaadin-tab>
            <vaadin-tab>
              <a href="#!/transferencias" @click="${this.cerrardrawer}">
                <iron-icon icon="vaadin:list"></iron-icon>
                TRANSFERENCIAS
              </a>
            </vaadin-tab>
            <vaadin-tab>
              <a href="#!/new" @click="${this.cerrardrawer}">
                <iron-icon icon="vaadin:options"></iron-icon>
                ABRE UNA CUENTA
              </a>
            </vaadin-tab>
            <vaadin-tab>
            <a href="#!/" @click="${this.logout}">
                <iron-icon icon="vaadin:exit"></iron-icon>
                LOGOUT
                </a>
            </vaadin-tab>
          </vaadin-tabs>
          <slot id="slot1">
            <div id="container">
              <div id="caja">
                ${this.route}
              </div>
            </div>


          </slot>
        </vaadin-app-layout>
        <iron-ajax id="ajax" @response="${this.bringdata1}"
          @error="${this.error}"
            url = "http://localhost:3000/api-peru/v1/login"
           method="post"
           handle-as = "json"
           content-type="application/json"
           >
        </iron-ajax>
        <iron-ajax id="ajax2" @response="${this.bringdata2}"
          @error="${this.error2}"
            url = "http://localhost:3000/api-peru/v1/logout"
           method="post"
           handle-as = "json"
           content-type="application/json"
           >
        </iron-ajax>
        <pacman-loader2 id="loader" class="ocultar"></pacman-loader2>
    `
  }
  setrouter(){
    var router = new Navigo('/', true, '#!')
    router
      .on('transactions', () => {
        this.route = html`
          <transaction-panel></transaction-panel>
        `
      })
      .on('transferencias', () => {
        this.route = html`
          <transferencia-page
            userID="${this.userID}"
            name="${this.name}"
            apepat="${this.apepat}"
            apemat="${this.apemat}">
          </transferencia-page>
        `
      })
      .on('pageb', () => {
        this.route = html`
          <confirmacion-page></confirmacion-page>
        `
      })
      .on('transferencias/confirm', () => {
        this.route = html`
          <confirmacion-page></confirmacion-page>
        `
      })
      .on('new', () => {
        this.route = html`
          <nueva-cuenta
            userID="${this.userID}"
            name="${this.name}"
            apepat="${this.apepat}"
            apemat="${this.apemat}"
          >
          </nueva-cuenta>
        `
      })
      .on('*', () => {
        this.route = html`
          <user-data
            userID="${this.userID}"
            name="${this.name}"
            apepat="${this.apepat}"
            apemat="${this.apemat}"
          ></user-data>
        `
      })


    router.resolve()
  }
  login(e){
    this.shadowRoot.getElementById("loader").classList.toggle('ocultar');
    let ajax=this.shadowRoot.getElementById("ajax");
    ajax.body={email : e.detail.username, password : e.detail.password}
    ajax.generateRequest();
  }
  error(){
    //if(!this.logged){
      setTimeout(()=>{
        this.shadowRoot.getElementById("loader").classList.toggle('ocultar');
        this.shadowRoot.getElementById("loginform").error = true;
      },2000)

    //}
    this.logged=false;
  }
  logout(){
    this.shadowRoot.getElementById("loginform").opened=true;
    this.shadowRoot.getElementById("drawer").drawerOpened=false;
    let ajax=this.shadowRoot.getElementById("ajax2");
    ajax.body={id : this.userID,email : this.email}
    ajax.generateRequest();
    sessionStorage.clear();
  }
  bringdata2(){
    console.log("logout correcto");
  }
  bringdata1(data){
    if(data.detail.status==200){
      this.userID=data.detail.response[0].userID;
      this.name=data.detail.response[0].first_name;
      this.apepat=data.detail.response[0].last_name;
      this.email =data.detail.response[0].email;
      this.setrouter();
      setTimeout(()=>{
          this.logged=true;
          this.shadowRoot.getElementById("loader").classList.toggle('ocultar');
          this.shadowRoot.getElementById("loginform").opened=false;
      },2000)
    }
  }
  cerrardrawer(){
    this.shadowRoot.getElementById("drawer").drawerOpened=false;
  }

}

customElements.define('principal-page', PrincipalPage);
