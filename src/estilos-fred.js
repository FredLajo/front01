import { css } from 'lit-element';

export const estilosfont = css`
  .blue{
    color: blue;
  }
  .red{
    color: red;
  }
  .black{
    color: black;
  }
  .white{
    color: white;
  }
  .grey{
    color:var(--lumo-secondary-text-color);
  }
  .s12{
    font-size:12px;
  }
  .s14{
    font-size:14px;
  }
  .s15{
    font-size:15px;
  }
  .s16{
    font-size:16px;
  }
  .s17{
    font-size:17px;
  }
  .ta_c{
    text-align:center;
  }
  .a_blank{
    text-decoration: none;
    text-decoration-color: none;
  }

  `;

export const contenedores = css`
    .contH{
      box-sizing: border-box;
      display:flex;
      flex-flow: row wrap;
    }
    .contV{
      display:flex;
      flex-flow: column nowrap;
      box-sizing: border-box;
    }
    .jc_c{
      justify-content: center;
    }
    .ai_c{
      align-items:center;
    }
    .ai_s{
      align-items: flex-start;
    }
    .ai_e{
      align-items: flex-end;
    }
    .jc_e{
      justify-content: flex-end;
    }
    .jc_sb{
      justify-content: space-between;
    }
    .jc_se{
      justify-content: space-evenly;
    }
    .w_15{
      width:15%;
    }
    .w_20{
      width:20%;
    }
    .w_30{
      width:30%;
    }
    .w_40{
      width:40%;
    }
    .w_50{
      width:50%;
    }
    .w_60{
      width:60%;
    }
    .w_70{
      width:70%;
    }
    .w_80{
      width:80%;
    }
    .w_90{
      width:90%;
    }
    .w_100{
      width:100%;
    }
    .h_100{
      height:100%;
    }
    .hidden {
        opacity: 0;
        transition: opacity 1s, height 0 1s;
        height: 0;
        display:none;
    }
    .m_10{
      margin:10px;
    }
    .m_t_10{
      margin-top:10px;
    }
    .m_t_5{
      margin-top:5px;
    }
    .b_b{
      box-sizing: border-box;
    }
    .p_r_10{
      padding-right:10px;
    }
    .p_l_20{
      padding-left:20px;
    }
    .p_t_10{
      padding-top:10px;
    }
    .p_b_10{
      padding-bottom:10px;
    }
    .p_10{
      padding:10px;
    }
    .fondo_azul{
      background-color: #18749F;
    }
    .br_s_5{
      border-radius:5px 5px 0px 0px;
    }
    .br_5{
      border-radius:5px
    }
    .br_estilo{
      border:1px solid var(--lumo-contrast-20pct);
    }

    `;

export const botones = css`
    .b_enviar{
      color: #fff;
      background-color: #337ab7;
      border-color: #2e6da4;
      cursor: pointer;
    }
    .b_confirmar{
      color: #fff;
      background-color: #3ABB0D;
      border-color: #329F0C;
      cursor: pointer;
    }
    .b_cancelar{
      color: #fff;
      background-color: #B6B5B5;
      border-color: #9F9A9A;
      cursor: pointer;
    }

`;
