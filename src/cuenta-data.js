import { LitElement, html } from 'lit-element';
import Navigo from 'https://unpkg.com/navigo@7.1.2/lib/navigo.es.js';
import { estilosfont,contenedores } from '/src/estilos-fred.js'


class CuentaData extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores
    ]}
  static get properties() {
    return {
      accountID: {type: String},
      accountNumber:{type:String},
      amountC:{type:String},
      amountD:{type:String},
      description:{type:String},
      cci:{type:String},
      divisa:{type:String}
    }
  }
  constructor(){
    super()

  }
  render() {
    return html`
    <style>
    .caja{
      height: 80px;
    }
    .caja:hover{
      background-color: rgba(179,232,239,0.6);
    }
    .cuenta{
      padding-left: 20px;
    }
    .saldo{
      padding-right: 30px;
    }

    </style>
      <a href="#!/transactions" class="caja s14 contH w_100 br_estilo a_blank" @click="${this.transacciones}" >
          <div class="cuenta contV jc_c ai_s w_60 h_100">
            <div class="blue s15" >${this.description}</div>
            <div class="black">${this.accountNumber}</div>
          </div>
          <div class="saldo contV jc_c ai_e w_40 h_100">
            <div class="blue">Contable</div>
            <div class="black">${this.divisa=='PEN'?'S/.':'$/.'} ${this.amountC}</div>
            <div class="blue">Disponible</div>
            <div class="black">${this.divisa=='PEN'?'S/.':'$/.'} ${this.amountD}</div>
          </div>
      </a>


    `
  }
  transacciones(){
    sessionStorage.setItem("accountID", this.accountID);
    sessionStorage.setItem("accountNumber", this.accountNumber);
    sessionStorage.setItem("cci", this.cci);
    sessionStorage.setItem("amountD", this.amountD);
    sessionStorage.setItem("amountC", this.amountC);
    sessionStorage.setItem("description", this.description);
  }
}

customElements.define('cuenta-data', CuentaData)
