import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class NuevaCuenta extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
    static get properties() {
      return {
        userID:{type:Number},
        name: { type: String },
        apepat: { type: String },
        idError: { type: Number },
        error: { type: String }
      }
    }
  constructor(){
    super();
    this.userID=0;
    this.name="";
    this.apepat="";
    this.idError=7;
    this.error="1";
  }
  render() {
    return html`
    <style>
      #crearCuentaPanel{
        background-color: white;
      }
      #logo{
        filter: grayscale(100%);
      }
    </style>
    <pantalla-mensaje id="alerta" errorID=${this.idError} error=${this.error} class="hidden"></pantalla-mensaje>
      <div id="crearCuentaPanel" class="contV w_100 ai_c p_b_10 ">
        <div class="contH w_100 jc_c ai_c">

          <img id="logo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQmo_ZmLM3rqDdkf2Y3jJ_GJ-bW5KAJu73eQ&usqp=CAU"
              alt = "FRED PRODUCTION" width="130px" height="170px"/>
          <div class="contV w_40 grey s14">
            <div class="s16 blue">Hola</div>
            <div class="s16 black">${this.name} ${this.apepat}</div>
            </br>
            <div>Abre una cuenta con nosotros de una manera facil, solo danos una descripción y nosotros nos encargamos del resto !!!</div>
          </div>

        </div>



        <vaadin-combo-box id="descripcion" label="Decripción" class="w_90 " value="" @change="${this.selectedCuenta}"
            items='[{"label": "Cuenta Ganadora", "value": "Cuenta Ganadora"},
            {"label": "Cuenta Corriente", "value": "Cuenta Corriente"},
            {"label": "Cuenta Sueldo", "value": "Cuenta Sueldo"}]'>
        </vaadin-combo-box>
        <vaadin-radio-group label="Moneda" theme="Horizontal" class="w_90">
          <vaadin-radio-button id="moneda" checked>Soles</vaadin-radio-button>
          <vaadin-radio-button >Dolares</vaadin-radio-button>
        </vaadin-radio-group>
        <div class="contH w_70 ai_c jc_se">
        <vaadin-button @click="${this.confirmar}" class="w_40 b_confirmar m_t_10">Confirmar</vaadin-button>
        <vaadin-button @click="${this.cancelar}" class="w_40 b_cancelar m_t_10">Cancelar</vaadin-button>
        </div>
      </div>
      <iron-ajax id="ajax" @response="${this.bringdata1}" @request="${this.cargando}"
        @error="${this.error}"
         url = "http://localhost:3000/api-peru/v1/users/${this.userID}/accounts/new"
         method = "put"
         handle-as = "json"
         content-type="application/json"
         >
      </iron-ajax>

    `
  }
  cargando(){
    this.idError=9;
    this.error="1";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
  }
  bringdata1(){
    this.idError=7;
    this.error="1";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
    setTimeout(()=>{
      this.shadowRoot.getElementById("alerta").classList.add('hidden');
      location.href = "#!/";
    },2000);
  }
  error(){
    this.idError=6;
    this.error="0";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
    setTimeout(()=>{
      this.shadowRoot.getElementById("alerta").classList.add('hidden');
    },2000);
  }
  confirmar(){
    let des=this.shadowRoot.getElementById("descripcion").value;
    if(this.verificarData(des)){
      let moneda=this.shadowRoot.getElementById("moneda");
      let divi="USD";
      if(moneda.checked){divi="PEN";}
      let ajax=this.shadowRoot.getElementById("ajax");
      ajax.body={description : des,divisa : divi};
      //console.log(ajax.body);
      ajax.generateRequest();
    }else{
      this.shadowRoot.getElementById("alerta").classList.remove('hidden');
      setTimeout(()=>{
        this.shadowRoot.getElementById("alerta").classList.add('hidden');
      },2000);
    }

  }
  cancelar(){
    location.href = "#!/";
  }
  verificarData(des){
    if(des==""){
      this.idError=8;
      this.error="0";
      return false;
    }
    return true;
  }
}

customElements.define('nueva-cuenta', NuevaCuenta)
