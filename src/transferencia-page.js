import { LitElement, html } from 'lit-element';
import { estilosfont,contenedores,botones } from '/src/estilos-fred.js'

class TransferenciaPage extends LitElement {
  static get styles() {
    return [
      estilosfont,
      contenedores,
      botones
    ]}
    static get properties() {
      return {
        userID:{type:String},
        data: {type: Object},
        name: { type: String },
        apepat: { type: String },
        apemat: { type: String },
        cambio: {type: Number},
        cuentasCargo: {type: Object},
        cuentaCargo:{type:String},
        cuentaAbono:{type:String},
        montoCargo:{type:Number},
        montoConvertido:{type:Number},
        montoDisponible:{type:Number},
        montoAbono:{type:Number},
        namecargo: { type: String },
        nameAbono: { type: String },
        divisa: { type: String },
        divisaCargo: { type: String },
        idError: { type: String },
        error: { type: String },
        tipoAbono: { type: String },
        tipoCambio:{type:Number},
        description: { type: String },
        referencia: { type: String },
        tipo:{type:Number}
      }
    }
  constructor(){
    super();
    this.userID="";
    this.data=[];
    this.cuentasCargo=[];
    this.cambio=3.3;
    this.nameCargo="";
    this.nameAbono="";
    this.divisa="PEN";
    this.divisaCargo="";
    this.cuentaCargo="";
    this.cuentaAbono="";
    this.montoCargo=0;
    this.montoDisponible=0;
    this.montoConvertido=0;
    this.montoAbono=0;
    this.idError=-1;
    this.error="0";
    this.tipoAbono="propio";
    this.tipoCambio=3.45;
    this.description="";
    this.referencia="";
    this.tipo=1;
  }
  render() {
    return html`
    <style>
      .bb{background-color: green}
      .aa{background-color: cyan}
    </style>
    <pantalla-mensaje id="alerta" errorID=${this.idError} error=${this.error} class="hidden"></pantalla-mensaje>
    <div id="transferencia" class="contV w_100 ">
        <vaadin-radio-group label="Transferencia" theme="Horizontal" class="w_100">
          <vaadin-radio-button @change="${this.prop}" checked>Cuenta propias</vaadin-radio-button>
          <vaadin-radio-button @change="${this.terc}">Cuentas de terceros</vaadin-radio-button>
        </vaadin-radio-group>

        <vaadin-combo-box id="comboCargo" label="Cuenta de cargo" class="w_100" @change="${this.selectedCargo}"></vaadin-combo-box>
        <div class="w_100 contH ai_c jc_e">
            <div class="grey p_r_10">Disponible</div>
            <div class="w_30  b_b ">
              <vaadin-text-field id="disponible"  class="w_100" readonly></vaadin-text-field>
            </div>
        </div>

        <div id="propio" class="w_100">
          <vaadin-combo-box @change="${this.selectedAbono}"
            id="comboAbono"
            label="Cuenta de abono"
            class="w_100"
            >
          </vaadin-combo-box>
        </div>
        <div id="tercero" class="w_100 hidden">
          <vaadin-radio-group label="Cuenta de abono" theme="Horizontal" class="w_100">
            <vaadin-radio-button id="propiobanco" @change="${this.loc}" checked>Propio banco</vaadin-radio-button>
            <vaadin-radio-button  @change="${this.inter}">Interbancaria</vaadin-radio-button>
          </vaadin-radio-group>

          <div id="local" class="w_100 contH ai_e jc_c ">
            <div class="w_20  b_b p_r_10">
              <vaadin-text-field  id="entidad" class="w_100" readonly></vaadin-text-field>
            </div>
            <div class="w_20  b_b p_r_10">
              <vaadin-text-field id="oficina" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_30  b_b p_r_10">
              <vaadin-text-field id="cuenta" class="w_100 " ></vaadin-text-field>
            </div>
          </div>

          <div id="interbancaria" class="w_100 contH ai_e jc_c hidden">
            <div class="w_15  b_b p_r_10">
              <vaadin-text-field id="ccientidad" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_15  b_b p_r_10">
              <vaadin-text-field id="ccioficina" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_40  b_b p_r_10">
              <vaadin-text-field id="ccicuenta" class="w_100" ></vaadin-text-field>
            </div>
            <div class="w_15  b_b p_r_10">
              <vaadin-text-field id="ccichequeo" class="w_100" ></vaadin-text-field>
            </div>
          </div>
        </div>



        <div class="w_100 contH">
          <div class="w_60  b_b p_r_10">
            <vaadin-combo-box  label="Moneda" class="w_100 " value="PEN" @change="${this.selectedDivisa}"
              items='[{"label": "Soles", "value": "PEN"},{"label": "Dolares", "value": "USD"}]'>
            </vaadin-combo-box>
          </div>
          <div class="w_40 ">
          <vaadin-number-field @change="${this.setMontoCargo}" id="montoCargo" label="Monto a transferir"
            step="0.01" min="0" max="1000" has-controls class="w_100" value="0">
          </vaadin-number-field>
          </div>
        </div>


        <vaadin-text-field id="referencia" label="Referencia de la operacion" class="w_100"></vaadin-text-field>
        <vaadin-button @click="${this.transferir}" class="w_100 b_enviar m_t_10">Transferir</vaadin-button>
    </div>
    <confirmacion-page id="confirmacion" class="hidden"
      cuentaCargo="${this.cuentaCargo}"
      cuentaAbono="${this.cuentaAbono}"
      divisa="${this.divisa}"
      divisaCargo="${this.divisaCargo}"
      montoCargo="${this.montoConvertido}"
      montoAbono="${this.montoAbono}"
      nameCargo="${this.nameCargo}"
      nameAbono="${this.nameAbono}"
      description="${this.referencia}"
      userID="${this.userID}"
      tipo="${this.tipo}"
    ></confirmacion-page>
    <vaadin-button id="cancelar_b" @click="${this.cancelar}" class="w_100 b_cancelar m_t_5 hidden">Cancelar</vaadin-button>
    <iron-ajax @response="${this.bringdata}" @request="${this.cargando}"
      @error="${this.error}"
       auto url = "http://localhost:3000/api-peru/v1/users/${this.userID}/accounts"
       handle-as = "json"

       >
    </iron-ajax>
    <!--datos de la cuenta beneficiario-->
    <iron-ajax id="ajax1" @response="${this.bringdata2}" @request="${this.cargando2}"
      @error="${this.error2}"
      url = "http://localhost:3000/api-peru/v1/accounts/users"
      method = "post"
      handle-as = "json"
      content-type="application/json"
       >
    </iron-ajax>


    `
  }
  error2(e){
    this.idError=10;
    this.error="0";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');
    setTimeout(()=>{
      this.shadowRoot.getElementById("alerta").classList.add('hidden');
    },2000);
    //console.log(e);
  }
  bringdata2(data){
    if(data.detail.response[0].divisa!=this.divisa){
      this.idError=13;
      this.error="0";
      this.shadowRoot.getElementById("alerta").classList.remove('hidden');
      setTimeout(()=>{
        this.shadowRoot.getElementById("alerta").classList.add('hidden');
      },3000);
    }else{
      this.shadowRoot.getElementById("alerta").classList.add('hidden');
      this.nameCargo=this.name+" "+this.apepat+" "+this.apemat;
      this.nameAbono=data.detail.response[0].first_name+" "+data.detail.response[0].last_name;
      this.montoAbono=this.montoCargo;
      if(this.divisaCargo!=this.divisa){
        if(this.divisaCargo=="USD"){
          this.montoConvertido=Math.round(this.montoCargo/this.tipoCambio * 100) / 100;

        }else{
          this.montoConvertido=this.montoCargo*this.tipoCambio;
        }
      }else{
        this.montoConvertido=this.montoCargo;
      }
      let ref=this.shadowRoot.getElementById("referencia").value;
      if(ref!=""){
        this.referencia=ref;
      }else{
        this.referencia=this.description;
      }


      this.shadowRoot.getElementById("transferencia").classList.add('hidden');
      this.shadowRoot.getElementById("confirmacion").classList.remove('hidden');
      this.shadowRoot.getElementById("cancelar_b").classList.remove('hidden');

    }


  }
  cargando2(){
    this.idError=9;
    this.error="1";
    this.shadowRoot.getElementById("alerta").classList.remove('hidden');

  }
  setMontoCargo(e){
    this.montoCargo=this.shadowRoot.getElementById("montoCargo").value;
  }
  selectedDivisa(e){
    this.divisa=e.target.value;
  }
  selectedCargo(e){
    this.data.forEach((combo)=> {
      if(combo.accountID==e.target.value){
        let moneda="S/. ";
        if(combo.divisa=="USD"){
          moneda="$/. ";
        }
        this.shadowRoot.getElementById("disponible").value=moneda+combo.balance;
        this.shadowRoot.getElementById("entidad").value=combo.cuenta.substr(0, 4);
        this.cuentaCargo=combo.cuenta;
        this.montoDisponible=combo.balance;
        this.divisaCargo=combo.divisa;
      }
    });

    this.setCuentaAbono(e.target.value);
  }
  setCuentaAbono(valor){
    let cuentasAbono=[];

    let comboBox=this.shadowRoot.getElementById("comboAbono");
    comboBox.value="";

    this.data.forEach((combo)=> {
      if(combo.accountID!=valor){
        var option = {"label":(combo.cuenta+' - '+combo.description),"value":combo.accountID}
        cuentasAbono.push(option);
      }
    });
    comboBox.items=cuentasAbono;
  }
  selectedAbono(e){
    this.data.forEach((combo)=> {
      if(combo.accountID==e.target.value){
        this.cuentaAbono=combo.cuenta;
      }
    });


  }
  bringdata(data){
    console.log("cargo de cuentas exitoso ...");
    let comboBox=this.shadowRoot.getElementById("comboCargo");
    this.data=data.detail.response;
    data.detail.response.forEach((combo)=> {
      var option = {"label":(combo.cuenta+' - '+combo.description),"value":combo.accountID}
      this.cuentasCargo.push(option);
    });
    comboBox.items=this.cuentasCargo;

  }
  inter(){
    this.shadowRoot.getElementById("interbancaria").classList.remove('hidden');
    this.shadowRoot.getElementById("local").classList.add('hidden');
    this.cuentaAbono="";
  }
  loc(){
    this.shadowRoot.getElementById("interbancaria").classList.add('hidden');
    this.shadowRoot.getElementById("local").classList.remove('hidden');
    this.cuentaAbono="";
  }
  prop(){
    this.shadowRoot.getElementById("tercero").classList.add('hidden');
    this.shadowRoot.getElementById("propio").classList.remove('hidden');
    this.tipoAbono="propio";
  }
  terc(){
    this.shadowRoot.getElementById("tercero").classList.remove('hidden');
    this.shadowRoot.getElementById("propio").classList.add('hidden');
    this.shadowRoot.getElementById("comboAbono").value="";
    this.tipoAbono="tercero";
  }
  transferir(){
    let errorr = this.verificarDatos();
    if(errorr<0){
      this.obtenerDataBeneficiario();
    }else{
      this.idError=errorr;
      this.error="0";
      this.shadowRoot.getElementById("alerta").classList.remove('hidden');
      setTimeout(()=>{
        this.shadowRoot.getElementById("alerta").classList.add('hidden');
      },2000);
    }
  }
  verificarDatos(){
    this.error="0";
    if(this.shadowRoot.getElementById("comboCargo").value==""){return 1;}
    if(this.tipoAbono=="propio"){
      if(this.shadowRoot.getElementById("comboAbono").value==""){return 2;}
    }else{
      if(this.shadowRoot.getElementById("propiobanco").checked){
        if(this.shadowRoot.getElementById("oficina").value.length!=4){return 2;}
        if(this.shadowRoot.getElementById("cuenta").value.length!=10){return 2;}
      }else{
        if(this.shadowRoot.getElementById("ccientidad").value.length!=3){return 2;}
        if(this.shadowRoot.getElementById("ccioficina").value.length!=3){return 2;}
        if(this.shadowRoot.getElementById("ccicuenta").value.length!=12){return 2;}
        if(this.shadowRoot.getElementById("ccichequeo").value.length!=2){return 2;}
      }
    }
    if(this.montoCargo==0){return 3;}
    if(this.divisaCargo!=this.divisa){
      if(this.divisaCargo=="USD"){
        if(this.montoCargo/this.tipoCambio>this.montoDisponible){
          return 0;
        }
      }else{
        if(this.montoCargo*this.tipoCambio>this.montoDisponible){
          return 0;
        }
      }
    }else{
      if(this.montoCargo>this.montoDisponible){
        return 0;
      }
    }

    return -1;
  }
  cancelar(){
    this.shadowRoot.getElementById("transferencia").classList.remove('hidden');
    this.shadowRoot.getElementById("confirmacion").classList.add('hidden');
    this.shadowRoot.getElementById("cancelar_b").classList.add('hidden');
  }
  obtenerDataBeneficiario(){
    let entidad="";
    let oficina="";
    let cuenta="";
    let chekeo="";
    this.tipo=1;
    let fullCuenta="";
    if(this.tipoAbono=="propio"){
      let valor=this.shadowRoot.getElementById("comboAbono").value;
      this.data.forEach((combo)=> {
        if(combo.accountID==valor){
          fullCuenta=combo.cuenta;
        }
      });
      this.description="Operacion entre cuentas propias";
    }else{
      if(this.shadowRoot.getElementById("propiobanco").checked){
        entidad=this.shadowRoot.getElementById("entidad").value;
        oficina=this.shadowRoot.getElementById("oficina").value;
        cuenta=this.shadowRoot.getElementById("cuenta").value;
        fullCuenta=entidad+"-"+oficina+"-"+cuenta;
        this.description="Operacion mismo banco";
      }else{
        entidad=this.shadowRoot.getElementById("ccientidad").value;
        oficina=this.shadowRoot.getElementById("ccioficina").value;
        cuenta=this.shadowRoot.getElementById("ccicuenta").value;
        chekeo=this.shadowRoot.getElementById("ccichequeo").value;
        fullCuenta=entidad+"-"+oficina+"-"+cuenta+"-"+chekeo;
        this.tipo=2;
        this.description="Operacion entre bancos";
      }
    }
    this.cuentaAbono=fullCuenta;
    let ajaxa=this.shadowRoot.getElementById("ajax1");
    ajaxa.body={cuenta : fullCuenta,tipo : this.tipo};
    //console.log(ajaxa.body);
    ajaxa.generateRequest();
  }
}

customElements.define('transferencia-page', TransferenciaPage)
